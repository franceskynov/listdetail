//
//  Friend.swift
//  ListDetail
//
//  Created by Dev on 10/18/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class Friend: NSObject {
    
    var name: String?
    var username: String?
    var nicname: String?
    
    init(name: String, username: String, nicname: String) {
        self.name = name
        self.username = username
        self.nicname = nicname
    }

}
